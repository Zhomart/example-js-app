const express = require('express')
const app = express()
const port = 3000

app.use(express.static('public'))

app.get('/api/get_images', (req, res) => {
    res.send(JSON.stringify([
        {title: "asd", url: "http://blah.jpg"}
    ]));
})

app.get('/*', (req, res) => {
    res.send('We are deeply sorry. But page 404.');
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
